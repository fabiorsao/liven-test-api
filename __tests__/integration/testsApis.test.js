/* eslint-disable no-console */
/* eslint-disable no-undef */
import { expect, describe } from '@jest/globals'
import {
  apiCreateUser,
  apiLogin,
  userUpdate,
  userRead,
  userDelete,
  addressCreate,
  addressUpdate,
  addressRead,
  addressDelete
} from './functionsApiAxios'

const faker = require('faker-br')

const email = faker.internet.email()
const password = faker.internet.password()
let token
let addressId

describe('Create User', () => {
  it('create user api test should return status 200', async () => {
    const response = await apiCreateUser(email, password)
    expect(response.status).toBe(200)
  })
})

describe('Authentication', () => {
  it('should received JWT token when authenticated with valid credentials', async () => {
    const response = await apiLogin(email, password)
    token = response.data.data.data.Authorization
    expect(response.status).toBe(200)
  })
})
describe('Update User', () => {
  it('should receive status 200 when user update is successful', async () => {
    const newEmail = faker.internet.email()
    const name = faker.name.firstName()
    const nickname = faker.name.lastName()
    const response = await userUpdate(token, name, nickname, newEmail)
    expect(response.status).toBe(200)
  })
})
describe('Read User', () => {
  it('should receive status 200 when user read is successful', async () => {
    const response = await userRead(token)
    expect(response.status).toBe(200)
  })
})
describe('Create Address', () => {
  it('should receive status 200 when address creation is successful', async () => {
    const country = faker.address.country()
    const state = faker.address.state()
    const city = faker.address.city()
    const completeAddress = faker.address.streetAddress()
    const address = faker.address.streetName()
    const response = await addressCreate(
      token,
      country,
      state,
      city,
      completeAddress,
      address
    )
    addressId = response.data.data.data.id

    expect(response.status).toBe(200)
  })
})
describe('Update Address', () => {
  it('should receive status 200 when address update is performed successfully', async () => {
    const country = faker.address.country()
    const state = faker.address.state()
    const city = faker.address.city()
    const completeAddress = faker.address.streetAddress()
    const address = faker.address.streetName()
    const response = await addressUpdate(
      token,
      country,
      state,
      city,
      completeAddress,
      address,
      addressId
    )
    expect(response.status).toBe(200)
  })
})
describe('Read Address', () => {
  it('should receive status 200 when address read is successful', async () => {
    const response = await addressRead(token, addressId)
    expect(response.status).toBe(200)
  })
})
describe('Delete Address', () => {
  it('should receive status 200 when address delete is successful', async () => {
    const response = await addressDelete(token, addressId)
    expect(response.status).toBe(200)
  })
})
describe('Delete User', () => {
  it('should receive status 200 when user delete is performed successfully', async () => {
    const response = await userDelete(token)
    expect(response.status).toBe(200)
  })
})
