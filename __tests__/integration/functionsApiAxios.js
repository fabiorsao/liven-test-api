const axios = require('axios')

// eslint-disable-next-line import/prefer-default-export
export const apiCreateUser = async (email, password) => {
  const options = {
    method: 'POST',
    url: 'http://localhost:8001/user/create',
    headers: { 'Content-Type': 'application/json' },
    data: {
      name: 'fabioefffe',
      nickname: 'nerso',
      email,
      password
    }
  }

  const response = await axios.request(options)
  return response
}

export const apiLogin = async (email, password) => {
  const options = {
    method: 'POST',
    url: 'http://localhost:8001/login',
    headers: { 'Content-Type': 'application/json' },
    data: { email, password }
  }

  const response = await axios.request(options)
  return response
}
export const userRead = async (authorization) => {
  const options = {
    method: 'GET',
    url: 'http://localhost:8001/user',
    headers: {
      authorization
    }
  }
  const response = await axios.request(options)
  return response
}
export const userUpdate = async (authorization, name, nickname, email) => {
  const options = {
    method: 'PATCH',
    url: 'http://localhost:8001/user/update',
    headers: {
      'Content-Type': 'application/json',
      authorization
    },
    data: { name, nickname, email }
  }
  const response = await axios.request(options)
  return response
}
export const userDelete = async (authorization) => {
  const options = {
    method: 'DELETE',
    url: 'http://localhost:8001/user/delete',
    headers: {
      authorization
    }
  }
  const response = await axios.request(options)
  return response
}
export const addressCreate = async (
  authorization,
  country,
  state,
  city,
  // eslint-disable-next-line camelcase
  complete_address,
  address
) => {
  const options = {
    method: 'POST',
    url: 'http://localhost:8001/address/create',
    headers: {
      'Content-Type': 'application/json',
      authorization
    },
    data: {
      country,
      state,
      city,
      complete_address,
      address
    }
  }
  const response = await axios.request(options)
  return response
}
export const addressUpdate = async (
  authorization,
  country,
  state,
  city,
  // eslint-disable-next-line camelcase
  complete_address,
  address,
  id
) => {
  const options = {
    method: 'PATCH',
    url: 'http://localhost:8001/address/update',
    headers: {
      'Content-Type': 'application/json',
      authorization
    },
    data: {
      id,
      country,
      state,
      city,
      complete_address,
      address
    }
  }
  const response = await axios.request(options)
  return response
}
export const addressRead = async (authorization, id) => {
  const options = {
    method: 'GET',
    url: 'http://localhost:8001/address/read',
    params: {
      id
    },
    headers: {
      'Content-Type': 'application/json',
      authorization
    }
  }
  const response = await axios.request(options)
  return response
}
export const addressDelete = async (authorization, id) => {
  const options = {
    method: 'DELETE',
    url: 'http://localhost:8001/address/delete',
    params: { id },
    headers: {
      authorization
    }
  }
  const response = await axios.request(options)
  return response
}
