import express from 'express'
import userController from '../controllers/user.js'
import authenticateToken from '../middleware/auth/index.js'
import inputValidate from '../middleware/inputValidate/user/index.js'

const routes = express.Router()

routes.get(
  '/user/all',
  [authenticateToken, inputValidate],
  userController.index
)
routes.get('/user', [authenticateToken, inputValidate], userController.show)
routes.post('/user/create', [inputValidate], userController.create)
routes.patch(
  '/user/update',
  [authenticateToken, inputValidate],
  userController.update
)
routes.delete(
  '/user/delete',
  [authenticateToken, inputValidate],
  userController.delete
)

export default routes
