import express from 'express'
import addressController from '../controllers/address.js'
import authenticateToken from '../middleware/auth/index.js'
import inputValidate from '../middleware/inputValidate/address/index.js'

const routes = express.Router()

// routes.get(
//   '/address',
//   [authenticateToken, inputValidate],
//   addressController.index
// )
routes.get('/address/read', [authenticateToken], addressController.show)
routes.post(
  '/address/create',
  [authenticateToken, inputValidate],
  addressController.create
)
routes.patch(
  '/address/update',
  [authenticateToken, inputValidate],
  addressController.update
)
routes.delete(
  '/address/delete',
  [authenticateToken, inputValidate],
  addressController.delete
)

export default routes
