import express from 'express'

import authRoute from './auth.js'
import passwordRoute from './password.js'
import addressRoute from './address.js'
import userRoute from './user.js'

const routes = express.Router()

routes.use(authRoute)
routes.use(passwordRoute)
routes.use(addressRoute)
routes.use(userRoute)

export default routes
