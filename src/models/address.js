import pkg from 'sequelize'

const { Model, DataTypes } = pkg

class Address extends Model {
  static init(sequelize) {
    super.init(
      {
        user_id: DataTypes.UUID,
        country: DataTypes.STRING,
        state: DataTypes.STRING,
        city: DataTypes.STRING,
        address: DataTypes.STRING,
        complete_address: DataTypes.STRING
      },
      {
        tableName: 'address',
        sequelize,
        modelName: 'address'
      }
    )
  }
}

export default Address
