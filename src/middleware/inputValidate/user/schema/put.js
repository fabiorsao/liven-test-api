import Joi from 'joi'

const validate = Joi.object({
  name: Joi.string().min(3).required(),
  email: Joi.string().email().required(),
  nickname: Joi.string().optional().empty('')
})

export default validate
