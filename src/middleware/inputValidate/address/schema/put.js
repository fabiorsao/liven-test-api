import Joi from 'joi'

const validate = Joi.object({
  id: Joi.string().uuid().required(),
  country: Joi.string().min(3).required(),
  state: Joi.string().min(2).required(),
  city: Joi.string().min(3).required(),
  address: Joi.string().min(3).required(),
  complete_address: Joi.string().min(3).required()
})

export default validate
