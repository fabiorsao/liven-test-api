import { Sequelize } from 'sequelize'
import dbConfig from './config/indexES6.js'

import Address from '../models/address.js'
import User from '../models/user.js'

// const env = process.env.NODE_ENV || 'local'

const connection = new Sequelize(dbConfig.local)

Address.init(connection)
User.init(connection)

export default connection
