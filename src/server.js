import './database/index.js'
import cors from 'cors'
import express from 'express'
import swaggerUi from 'swagger-ui-express'
import routes from './routes/index.js'
import swaggerDocument from './swagger.json'

const env = process.env.NODE_ENV || 'local'
const nodeEnvUpper = env.toUpperCase()

const API_HOST = process.env[`${nodeEnvUpper}_API_HOST`]
const API_PORT = process.env[`${nodeEnvUpper}_API_PORT`]

const app = express()

app.use(express.json())

app.use(cors())

app.use((err, req, res, next) => {
  if (err instanceof SyntaxError) {
    return res
      .status(err.status)
      .send({ status: err.status, message: err.message })
  }
  return next()
})

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

app.use(routes)

const server = app.listen(API_PORT, API_HOST, () =>
  console.log('API port %d in %s mode %s', API_PORT, API_HOST, nodeEnvUpper)
)
export default server
