import { responseClient, errorResponse } from '../utils/response.js'
import Address from '../models/address.js'
import User from '../models/user.js'

const show = 'addres'
const index = 'address'

export default {
  async index(req, res) {
    try {
      let data = []

      data = await Address.findAll({
        attributes: { exclude: ['createdAt', 'updatedAt'] },
        order: [['name', 'ASC']]
      })
      responseClient(res, {
        error: false,
        message: `${index} founded`,
        data
      })
    } catch (error) {
      errorResponse(res, error)
    }
  },

  async show(req, res) {
    try {
      const filter = req.query
      filter.user_id = req.user.id
      const data = await Address.findAll({
        where: filter,
        attributes: { exclude: ['createdAt', 'updatedAt'] }
      })

      if (!data) {
        throw { code: 400, message: `${show} not founded` }
      }

      responseClient(res, {
        error: false,
        message: `${show} founded`,
        data
      })
    } catch (error) {
      errorResponse(res, error)
    }
  },

  async create(req, res) {
    try {
      const isRegisteredUser = await User.findOne({
        where: { id: req.user.id }
      })

      if (!isRegisteredUser) {
        throw { code: 400, message: 'User not founded' }
      }

      const data = await Address.create({
        user_id: isRegisteredUser.id,
        country: req.body.country,
        state: req.body.state,
        city: req.body.city,
        address: req.body.address,
        complete_address: req.body.complete_address
      })

      responseClient(res, {
        error: false,
        message: `${show} created`,
        data: { id: data.id }
      })
    } catch (error) {
      errorResponse(res, error)
    }
  },

  async update(req, res) {
    try {
      if (req.body.id) {
        const isRegisteredUser = await Address.findOne({
          where: { id: req.body.id }
        })

        if (!isRegisteredUser) {
          throw { code: 400, message: 'Address not founded' }
        }
      }

      const [isRegistered, rowAffected] = await Address.update(req.body, {
        where: {
          id: req.body.id
        },
        returning: true
      })

      if (!isRegistered) {
        throw { code: 400, message: `${show} not founded` }
      }

      const data = rowAffected?.[0]?.dataValues

      responseClient(res, {
        error: false,
        message: `${show} updated`,
        data: { id: data.id }
      })
    } catch (error) {
      errorResponse(res, error)
    }
  },

  async delete(req, res) {
    try {
      const data = await Address.destroy({
        where: {
          id: req.query.id.split(',')
        }
      })

      if (!data) {
        throw { code: 400, message: `${show} not founded` }
      }

      responseClient(res, {
        error: false,
        message: `${show} deleted`,
        data: { id: req.query.id }
      })
    } catch (error) {
      errorResponse(res, error)
    }
  }
}
