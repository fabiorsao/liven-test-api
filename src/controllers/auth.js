import User from '../models/user.js'
import { validatePassword, createToken } from '../utils/auth.js'
import { responseClient, errorResponse } from '../utils/response.js'

export default {
  async login(req, res) {
    try {
      const { email, password } = req.body

      const user = await User.findOne({
        where: { email },
        attributes: { exclude: ['createdAt', 'updatedAt'] }
      })
      if (!user) {
        throw { code: 400, message: 'User not found' }
      }

      const isValid = await validatePassword(password, user.password)
      if (!isValid) {
        throw { code: 400, message: 'Invalid email/password' }
      }

      const Authorization = createToken({
        user: {
          id: user.id,
          name: user.name,
          nickname: user.nickname,
          email: user.email
        }
      })

      responseClient(res, {
        error: false,
        message: 'Login success',
        data: {
          Authorization: `Bearer ${Authorization}`,
          userId: user.id
        }
      })
    } catch (error) {
      errorResponse(res, error)
    }
  },
  async logout(req, res) {
    try {
      // if (!authorization) {
      //   throw { code: 401, message: 'Authorization is required' }
      // }

      responseClient(res, { error: false, message: 'Logout success' })
    } catch (error) {
      errorResponse(res, error)
    }
  }
}
