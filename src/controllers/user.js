import pkg from 'sequelize'
import { responseClient, errorResponse } from '../utils/response.js'
import User from '../models/user.js'
import Address from '../models/address.js'
import { encryptPassword } from '../utils/auth.js'

const show = 'user'
const index = 'users'

const { Op } = pkg

export default {
  async index(req, res) {
    try {
      let data = []
      data = await User.findAll({
        attributes: {
          exclude: ['createdAt', 'updatedAt', 'password']
        },
        order: [['name', 'ASC']]
      })

      responseClient(res, {
        error: false,
        message: `${index} founded`,
        data
      })
    } catch (error) {
      errorResponse(res, error)
    }
  },

  async show(req, res) {
    try {
      const userData = await User.findOne({
        where: {
          id: req.user.id
        },
        attributes: {
          exclude: ['createdAt', 'updatedAt', 'password']
        }
      })
      const AddressData = await Address.findAll({
        where: {
          user_id: req.user.id
        },
        attributes: { exclude: ['createdAt', 'updatedAt'] }
      })

      if (!userData) {
        throw { code: 400, message: `${show} not founded` }
      }
      const data = {
        userData,
        AddressData
      }

      responseClient(res, {
        error: false,
        message: `${show} founded`,
        data
      })
    } catch (error) {
      errorResponse(res, error)
    }
  },

  async create(req, res) {
    try {
      const { password } = req.body
      const hashPassword = await encryptPassword(password)

      const [data, isNew] = await User.findCreateFind({
        where: {
          email: req.body.email
        },
        defaults: {
          name: req.body.name,
          nickname: req.body.nickname,
          email: req.body.email,
          password: hashPassword
        }
      })

      if (!isNew) {
        throw { code: 400, message: 'Email already registered' }
      }

      responseClient(res, {
        error: false,
        message: `${show} created`,
        data: { id: data.id }
      })
    } catch (error) {
      errorResponse(res, error)
    }
  },

  async update(req, res) {
    try {
      const { email } = req.body
      const { id } = req.user

      if (email) {
        const usersWithSameEmail = await User.findOne({
          where: {
            id: { [Op.ne]: id },
            email: { [Op.eq]: email }
          }
        })
        if (usersWithSameEmail) {
          throw { code: 400, message: 'Email already registered' }
        }
      }

      const [isRegistered, rowAffected] = await User.update(req.body, {
        where: {
          id
        },
        returning: true
      })

      if (!isRegistered) {
        throw { code: 400, message: `${show} not founded` }
      }

      const data = rowAffected?.[0]?.dataValues

      responseClient(res, {
        error: false,
        message: `${show} updated`,
        data: { id: data.id }
      })
    } catch (error) {
      errorResponse(res, error)
    }
  },

  async delete(req, res) {
    try {
      const data = await User.destroy({
        where: {
          id: req.user.id.split(',')
        }
      })

      if (!data) {
        throw { code: 400, message: `${show} not founded` }
      }

      responseClient(res, {
        error: false,
        message: `${show} deleted`,
        data: { id: req.user.id }
      })
    } catch (error) {
      errorResponse(res, error)
    }
  }
}
