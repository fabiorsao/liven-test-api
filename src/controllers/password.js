import User from '../models/user.js'
import { encryptPassword, getFromToken } from '../utils/auth.js'
import { responseClient, errorResponse } from '../utils/response.js'

export default {
  async forgot(req, res) {
    try {
      const user = await User.findOne({
        where: { email: req.body.email }
      })

      if (!user) {
        throw { code: 400, message: 'User not founded' }
      }

      responseClient(res, {
        error: false,
        message: user
      })
    } catch (error) {
      errorResponse(res, error)
    }
  },
  async new(req, res) {
    try {
      const { password } = req.body

      const { user } = await getFromToken(req.headers.authorization, ['user'])

      const hashPassword = await encryptPassword(password)

      const [isRegistered] = await User.update(
        { password: hashPassword },
        {
          where: {
            id: user.id
          },
          returning: true
        }
      )

      if (!isRegistered) {
        throw { code: 400, message: `User not founded` }
      }

      responseClient(res, {
        error: false,
        message: `Password change successfully`
      })
    } catch (error) {
      errorResponse(res, error)
    }
  }
}
