
# Liven API users, address

Api node express with routes to create, edit, view, delete users and their addresses.
Authentication via JWT token.


## Features

- Users API
- API addresses
- Swagger documentation
- Jest tests
- json insomnia
- docker image postgres
- sequelize
- migrations

## Upload a docker instance
Inside the project folder, run the command

```shell
make up
```

To see docker logs, run the command (optional):

```shell
make logs
```

To kill docker instance, run the command (optional):

```shell
make down
```
### 4. Run database migrations

Inside the project folder, run the command

```shell
npx sequelize db:migrate
```

## Run API

Inside the project folder, run the command

```bash
  git clone https://gitlab.com/fabiorsao/liven-test-api.git
```

Enter the project directory

```bash
  cd livenTestAPI
```

Install the dependencies

```bash
  npm install
```

Start the server

```bash
  npm run start
```

## Run Tests Jest

Inside the project folder, run the command

```bash
  npm test
```

## Screenshots

![App Screenshot](https://i.ibb.co/k1Wt4Hp/Captura-de-tela-2022-05-09-232143.png)
![App Screenshot](https://i.ibb.co/7SMQGBB/Captura-de-tela-2022-05-09-232424.png)
![App Screenshot](https://i.ibb.co/MGmnkqm/Captura-de-tela-2022-05-09-232623.png)
tests jest print
![App Screenshot](https://i.ibb.co/6w07pbD/tests.png)
